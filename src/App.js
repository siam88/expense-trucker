import React from 'react';
import './App.css';
import Header from './components/header';
import Balance from './components/balance';
import IncomeExpenses from './components/incomeExpenses';
import Transaction from './components/transactions';
import { GlobalProvider } from './context/GlobalState';

function App() {
  return (
    <GlobalProvider>
      <Header />
      <div className="container">
        <Balance />
        <IncomeExpenses />
        <Transaction />
      </div>
    </GlobalProvider>

  );
}

export default App;
