import React, { Fragment } from "react";
import TransactionHistory from "./transactionHistory";
import AddTransaction from './addTransaction'

const Transaction = () => {
    return (
        <Fragment>
            <TransactionHistory />
            <AddTransaction />
        </Fragment>
    )
}

export default Transaction;